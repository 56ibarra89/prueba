/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Nomina;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sistemas28
 */
public class Empleado {
    int id;
    String carnet,nombre,apellido,cedula,direccion,telefono;
    double Salario_neto;
    String Avatar;

    public Empleado(int id, String carnet, String nombre, String apellido, String cedula, String direccion, String telefono, double Salario_neto, String Avatar) {
        this.id = id;
        this.carnet = carnet;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.direccion = direccion;
        this.telefono = telefono;
        this.Salario_neto = Salario_neto;
        this.Avatar = Avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public double getSalario_neto() {
        return Salario_neto;
    }

    public void setSalario_neto(double Salario_neto) {
        this.Salario_neto = Salario_neto;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }
    
     public List<String> toList() {
        List<String> empleado = new ArrayList<>();

        empleado.add(String.valueOf(id));
        empleado.add(nombre);
        empleado.add(apellido);
        empleado.add(cedula);
        empleado.add(direccion);
        empleado.add(telefono);
        empleado.add(carnet);

        return empleado;

    }
    
    
}
