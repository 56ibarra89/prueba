/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Nomina;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Sistemas28
 */
public class EmpleadoTablaModel {
    private List<String> columnNames;
    private List<Empleado> data;

    public EmpleadoTablaModel() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
    }

    public EmpleadoTablaModel(List<String> ColumnNames, List<Empleado> data) {
        this.columnNames = ColumnNames;
        this.data = data;
    }

    public int getRowCount() {
        return data.size();
    }

   
    public int getColumnCount() {
        return columnNames.size();
    }

    
    public Object getValueAt(int row, int col) {

        if (data.isEmpty()) {
            return null;
        }

        if (row < 0 || row >= data.size()) {
            return null;
        }

        List<String> empleado = data.get(row).toList();

        if (col < 0 || col >= empleado.size()) {
            return null;
        }

        return empleado.get(col);
    }

    public String getColumnName(int column) {
        return columnNames.get(column);
    }

   
    public void loadFromJson() throws FileNotFoundException {
        Gson gson = new Gson();
        data.addAll(Arrays.asList(gson.fromJson(new FileReader("Empleado.json"), Empleado[].class)));
        String[] names = {"id", "Nombres", "Apellidos","carnet", "Cedula", "Direccion","salirio", "Telefono", "Avatar"};
        columnNames = Arrays.asList(names);
    }

}
